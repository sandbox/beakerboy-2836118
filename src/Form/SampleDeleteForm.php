<?php

namespace Drupal\lab_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Sample entities.
 *
 * @ingroup lab_system
 */
class SampleDeleteForm extends ContentEntityDeleteForm {


}
