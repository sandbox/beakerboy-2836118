<?php

namespace Drupal\lab_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Chemical Analysis entities.
 *
 * @ingroup lab_system
 */
class ChemicalAnalysisDeleteForm extends ContentEntityDeleteForm {


}
