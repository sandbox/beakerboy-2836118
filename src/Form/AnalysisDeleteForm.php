<?php

namespace Drupal\lab_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Analysis entities.
 *
 * @ingroup lab_system
 */
class AnalysisDeleteForm extends ContentEntityDeleteForm {


}
