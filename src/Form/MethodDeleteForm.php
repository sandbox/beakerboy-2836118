<?php

namespace Drupal\lab_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Method entities.
 *
 * @ingroup lab_system
 */
class MethodDeleteForm extends ContentEntityDeleteForm {


}
