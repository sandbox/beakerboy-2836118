<?php

namespace Drupal\lab_system\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Chemical Analysis edit forms.
 *
 * @ingroup lab_system
 */
class ChemicalAnalysisForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\lab_system\Entity\ChemicalAnalysis */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Chemical Analysis.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Chemical Analysis.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.chemical_analysis.canonical', ['chemical_analysis' => $entity->id()]);
  }

}
