<?php

namespace Drupal\lab_system\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for ChemicalResult edit forms.
 *
 * @ingroup lab_system
 */
class ChemicalResultForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\lab_system\Entity\ChemicalResult */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label ChemicalResult.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label ChemicalResult.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.chemical_result.canonical', ['chemical_result' => $entity->id()]);
  }

}
