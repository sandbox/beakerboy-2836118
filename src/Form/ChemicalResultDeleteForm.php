<?php

namespace Drupal\lab_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting ChemicalResult entities.
 *
 * @ingroup lab_system
 */
class ChemicalResultDeleteForm extends ContentEntityDeleteForm {


}
