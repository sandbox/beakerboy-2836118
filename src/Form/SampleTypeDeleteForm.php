<?php

namespace Drupal\lab_system\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting SampleType entities.
 *
 * @ingroup lab_system
 */
class SampleTypeDeleteForm extends ContentEntityDeleteForm {


}
