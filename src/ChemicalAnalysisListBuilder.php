<?php

namespace Drupal\lab_system;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Chemical Analysis entities.
 *
 * @ingroup lab_system
 */
class ChemicalAnalysisListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Chemical Analysis ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\lab_system\Entity\ChemicalAnalysis */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.chemical_analysis.edit_form', array(
          'chemical_analysis' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
