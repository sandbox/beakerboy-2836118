<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining  Analysis entities.
 *
 * @ingroup lab_system
 */
interface AnalysisInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

}
