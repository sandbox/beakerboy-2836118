<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Sample entities.
 *
 * @ingroup lab_system
 */
interface SampleInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Sample name.
   *
   * @return string
   *   Name of the Sample.
   */
  public function getName();

  /**
   * Sets the Sample name.
   *
   * @param string $name
   *   The Sample name.
   *
   * @return \Drupal\lab_system\Entity\SampleInterface
   *   The called Sample entity.
   */
  public function setName($name);
}
