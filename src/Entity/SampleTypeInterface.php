<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Sample Type entities.
 *
 * @ingroup lab_system
 */
interface SampleTypeInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Sample Type name.
   *
   * @return string
   *   Name of the Sample Type.
   */
  public function getName();

  /**
   * Sets the Sample Type name.
   *
   * @param string $name
   *   The Sample Type name.
   *
   * @return \Drupal\lab_system\Entity\SampleTypeInterface
   *   The called SampleType entity.
   */
  public function setName($name);

}
