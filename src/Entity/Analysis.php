<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the  Analysis entity.
 *
 * @ingroup lab_system
 *
 * @ContentEntityType(
 *   id = "analysis",
 *   label = @Translation("Analysis"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lab_system\AnalysisListBuilder",
 *     "views_data" = "Drupal\lab_system\Entity\AnalysisViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\lab_system\Form\AnalysisForm",
 *       "add" = "Drupal\lab_system\Form\AnalysisForm",
 *       "edit" = "Drupal\lab_system\Form\AnalysisForm",
 *       "delete" = "Drupal\lab_system\Form\AnalysisDeleteForm",
 *     },
 *     "access" = "Drupal\lab_system\AnalysisAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\lab_system\AnalysisHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "analysis",
 *   admin_permission = "administer analysis entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "note" = "note",
 *     "date" = "date",
 *     "method_id" = "method_id",
 *     "sample_id" = "sample_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/lab_system/analysis/{analysis}",
 *     "add-form" = "/admin/structure/lab_system/analysis/add",
 *     "edit-form" = "/admin/structure/lab_system/analysis/{analysis}/edit",
 *     "delete-form" = "/admin/structure/lab_system/analysis/{analysis}/delete",
 *     "collection" = "/admin/structure/lab_system/analysis",
 *   },
 *   field_ui_base_route = "analysis.settings"
 * )
 */
class Analysis extends ContentEntityBase implements AnalysisInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['note'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Note'))
      ->setDescription(t(' Analysis Note.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Date'))
      ->setDescription(t('Date analysis was performed.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['method_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Method'))
      ->setDescription(t('Method.'))
      ->setSettings(array(
        'target_type' => 'method',
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['result'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Results'))
      ->setDescription(t('Results.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'result',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'result',
        'weight'   => 5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    return $fields;
  }

}
