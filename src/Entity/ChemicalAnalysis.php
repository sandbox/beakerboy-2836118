<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Chemical Analysis entity.
 *
 * @ingroup lab_system
 *
 * @ContentEntityType(
 *   id = "chemical_analysis",
 *   label = @Translation("Chemical Analysis"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lab_system\ChemicalAnalysisListBuilder",
 *     "views_data" = "Drupal\lab_system\Entity\ChemicalAnalysisViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\lab_system\Form\ChemicalAnalysisForm",
 *       "add" = "Drupal\lab_system\Form\ChemicalAnalysisForm",
 *       "edit" = "Drupal\lab_system\Form\ChemicalAnalysisForm",
 *       "delete" = "Drupal\lab_system\Form\ChemicalAnalysisDeleteForm",
 *     },
 *     "access" = "Drupal\lab_system\ChemicalAnalysisAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\lab_system\ChemicalAnalysisHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "chemical_analysis",
 *   admin_permission = "administer chemical_analysis entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "note" = "note",
 *     "date" = "date",
 *     "method" = "method",
 *     "chemical_result" = "chemical_result",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/lab_system/chemical_analysis/{chemical_analysis}",
 *     "add-form" = "/admin/structure/lab_system/chemical_analysis/add",
 *     "edit-form" = "/admin/structure/lab_system/chemical_analysis/{chemical_analysis}/edit",
 *     "delete-form" = "/admin/structure/lab_system/chemical_analysis/{chemical_analysis}/delete",
 *     "collection" = "/admin/structure/lab_system/chemical_analysis",
 *   },
 *   field_ui_base_route = "chemical_analysis.settings"
 * )
 */
class ChemicalAnalysis extends ContentEntityBase implements ChemicalAnalysisInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['note'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Note'))
      ->setDescription(t('Chemical Analysis Note.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Date'))
      ->setDescription(t('Date analysis was performed.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['method'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Method'))
      ->setDescription(t('Method.'))
      ->setSettings(array(
        'target_type' => 'method',
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['chemical_result'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Results'))
      ->setDescription(t('Results.'))
      ->setSettings(array(
        'target_type' => 'chemical_result',
      ))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'chemical_result',
        'weight'   => 5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    return $fields;
  }

}
