<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the  ChemicalResult entity.
 *
 * @ingroup lab_system
 *
 * @ContentEntityType(
 *   id = "chemical_result",
 *   label = @Translation("ChemicalResult"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lab_system\ChemicalResultListBuilder",
 *     "views_data" = "Drupal\lab_system\Entity\ChemicalResultViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\lab_system\Form\ChemicalResultForm",
 *       "add" = "Drupal\lab_system\Form\ChemicalResultForm",
 *       "edit" = "Drupal\lab_system\Form\ChemicalResultForm",
 *       "delete" = "Drupal\lab_system\Form\ChemicalResultDeleteForm",
 *     },
 *     "access" = "Drupal\lab_system\ChemicalResultAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\lab_system\ChemicalResultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "chemical_result",
 *   admin_permission = "administer chemical_result entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "note" = "note",
 *     "date" = "date",
 *     "method_id" = "method_id",
 *     "sample_id" = "sample_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/lab_system/chemical_result/{chemical_result}",
 *     "add-form" = "/admin/structure/lab_system/chemical_result/add",
 *     "edit-form" = "/admin/structure/lab_system/chemical_result/{chemical_result}/edit",
 *     "delete-form" = "/admin/structure/lab_system/chemical_result/{chemical_result}/delete",
 *     "collection" = "/admin/structure/lab_system/chemical_result",
 *   },
 *   field_ui_base_route = "chemical_result.settings"
 * )
 */
class ChemicalResult extends ContentEntityBase implements ChemicalResultInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['chemical'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Chemical'))
      ->setDescription(t('Chemical.'))
      ->setSettings(array(
        'target_type' => 'chemical',
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['result'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Results'))
      ->setDescription(t('Results.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'result',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'result',
        'weight'   => 5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    return $fields;
  }

}
