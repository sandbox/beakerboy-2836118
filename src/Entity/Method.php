<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Method entity.
 *
 * @ingroup lab_system
 *
 * @ContentEntityType(
 *   id = "method",
 *   label = @Translation("Method"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lab_system\MethodListBuilder",
 *     "views_data" = "Drupal\lab_system\Entity\MethodViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\lab_system\Form\MethodForm",
 *       "add" = "Drupal\lab_system\Form\MethodForm",
 *       "edit" = "Drupal\lab_system\Form\MethodForm",
 *       "delete" = "Drupal\lab_system\Form\MethodDeleteForm",
 *     },
 *     "access" = "Drupal\lab_system\MethodAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\lab_system\MethodHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "method",
 *   admin_permission = "administer method entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "description" = "description",
 *     "units" = "units",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/lab_system/method/{method}",
 *     "add-form" = "/admin/structure/lab_system/method/add",
 *     "edit-form" = "/admin/structure/lab_system/method/{method}/edit",
 *     "delete-form" = "/admin/structure/lab_system/method/{method}/delete",
 *     "collection" = "/admin/structure/lab_system/method",
 *   },
 *   field_ui_base_route = "method.settings"
 * )
 */
class Method extends ContentEntityBase implements MethodInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getUnits() {
    return $this->get('units')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Method.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Method description.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['units'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Units'))
      ->setDescription(t('Units of the method results.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
