<?php

namespace Drupal\lab_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Chemical Analysis entities.
 */
class ChemicalAnalysisViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['chemical_analysis']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Chemical Analysis'),
      'help' => $this->t('The Chemical Analysis ID.'),
    );

    return $data;
  }

}
