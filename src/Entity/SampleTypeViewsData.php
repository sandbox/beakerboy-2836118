<?php

namespace Drupal\lab_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for SampleType entities.
 */
class SampleTypeViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['sample_type']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Sample Type'),
      'help' => $this->t('The Sample Type ID.'),
    );

    return $data;
  }

}
