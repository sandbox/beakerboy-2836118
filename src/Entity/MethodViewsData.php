<?php

namespace Drupal\lab_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Method entities.
 */
class MethodViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['method']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Method'),
      'help' => $this->t('The Method ID.'),
    );

    return $data;
  }

}
