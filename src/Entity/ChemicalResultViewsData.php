<?php

namespace Drupal\lab_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for ChemicalResult entities.
 */
class ChemicalResultViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['chemical_result']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('ChemicalResult'),
      'help' => $this->t('The ChemicalResult ID.'),
    );

    return $data;
  }

}
