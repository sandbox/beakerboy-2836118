<?php

namespace Drupal\lab_system\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Sample entities.
 */
class SampleViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['sample']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Sample'),
      'help' => $this->t('The Sample ID.'),
    );

    return $data;
  }

}
