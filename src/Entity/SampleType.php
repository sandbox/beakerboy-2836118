<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Sample Type entity.
 *
 * @ingroup lab_system
 *
 * @ContentEntityType(
 *   id = "sample_type",
 *   label = @Translation("Sample Type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lab_system\SampleTypeListBuilder",
 *     "views_data" = "Drupal\lab_system\Entity\SampleTypeViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\lab_system\Form\SampleTypeForm",
 *       "add" = "Drupal\lab_system\Form\SampleTypeForm",
 *       "edit" = "Drupal\lab_system\Form\SampleTypeForm",
 *       "delete" = "Drupal\lab_system\Form\SampleTypeDeleteForm",
 *     },
 *     "access" = "Drupal\lab_system\SampleTypeAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\lab_system\SampleTypeHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "sample_type",
 *   admin_permission = "administer sample_type entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "description" = "description",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/lab_system/sample_type/{sample_type}",
 *     "add-form" = "/admin/structure/lab_system/sample_type/add",
 *     "edit-form" = "/admin/structure/lab_system/sample_type/{sample_type}/edit",
 *     "delete-form" = "/admin/structure/lab_system/sample_type/{sample_type}/delete",
 *     "collection" = "/admin/structure/lab_system/sample_type",
 *   },
 *   field_ui_base_route = "sample_type.settings"
 * )
 */
class SampleType extends ContentEntityBase implements SampleTypeInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Sample Type.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Sample Type description.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
