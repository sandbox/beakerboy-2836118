<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Chemical Analysis entities.
 *
 * @ingroup lab_system
 */
interface ChemicalAnalysisInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

}
