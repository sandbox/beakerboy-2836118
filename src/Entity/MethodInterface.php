<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Method entities.
 *
 * @ingroup lab_system
 */
interface MethodInterface extends ContentEntityInterface {
  /**
   * Gets the Method name.
   *
   * @return string
   *   Name of the Method.
   */
  public function getName();

  /**
   * Sets the Method name.
   *
   * @param string $name
   *   The Method name.
   *
   * @return \Drupal\lab_system\Entity\MethodInterface
   *   The called Method entity.
   */
  public function setName($name);

}
