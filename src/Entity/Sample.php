<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Sample entity.
 *
 * @ingroup lab_system
 *
 * @ContentEntityType(
 *   id = "sample",
 *   label = @Translation("Sample"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\lab_system\SampleListBuilder",
 *     "views_data" = "Drupal\lab_system\Entity\SampleViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\lab_system\Form\SampleForm",
 *       "add" = "Drupal\lab_system\Form\SampleForm",
 *       "edit" = "Drupal\lab_system\Form\SampleForm",
 *       "delete" = "Drupal\lab_system\Form\SampleDeleteForm",
 *     },
 *     "access" = "Drupal\lab_system\SampleAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\lab_system\SampleHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "sample",
 *   admin_permission = "administer sample entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "lot",
 *     "description" = "description",
 *     "date" = "date",
 *     "location" = "location",
 *     "sample_type_id" = "sample_type_id",
 *     "chemical_analysis_id" = "chemical_analysis_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/lab_system/sample/{sample}",
 *     "add-form" = "/admin/structure/lab_system/sample/add",
 *     "edit-form" = "/admin/structure/lab_system/sample/{sample}/edit",
 *     "delete-form" = "/admin/structure/lab_system/sample/{sample}/delete",
 *     "collection" = "/admin/structure/lab_system/sample",
 *   },
 *   field_ui_base_route = "sample.settings"
 * )
 */
class Sample extends ContentEntityBase implements SampleInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('lot')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($lot) {
    $this->set('lot', $lot);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['lot'] = BaseFieldDefinition::create('string')
      ->setLabel(t('LOT'))
      ->setDescription(t('The lot number of the Sample.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Sample description.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['location'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Location'))
      ->setDescription(t('Location from which the sample was taken.'))
      ->setSettings(array(
        'max_length' => 50,
        'text_processing' => 0,
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Date'))
      ->setDescription(t('Date sample was taken.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'datetime_timestamp',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['sample_type_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Sample Type'))
      ->setDescription(t('Sample Type.'))
      ->setSettings(array(
        'target_type' => 'sample_type',
      ))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['analysis'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Analysis'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings(array(
        'target_type' => 'analysis',
      ))
      ->setDescription(t('Sample Analyses.'))
      ->setDisplayOptions('view', array(
        'type'     => 'entity_reference_autocomplete',
        'weight'   => 5,
        'settings' => array(
          'match_operator'    => 'CONTAINS',
          'size'              => '60',
          'autocomplete_type' => 'tags',
          'placeholder'       => '',
        ),
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'inline_entity_form_complex',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['chemical_analysis_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Chemical Analysis'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSettings(array(
        'target_type' => 'chemical_analysis',
      ))
      ->setDescription(t('Chemical Analyses.'))
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'analysis',
        'weight' => -4,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'weight' => -4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    return $fields;
  }

}
