<?php

namespace Drupal\lab_system\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining  ChemicalResult entities.
 *
 * @ingroup lab_system
 */
interface ChemicalResultInterface extends ContentEntityInterface {

  // Add get/set methods for your configuration properties here.

}
