<?php

namespace Drupal\lab_system;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Sample Type entity.
 *
 * @see \Drupal\lab_system\Entity\SampleType.
 */
class SampleTypeAccessControlHandler extends EntityAccessControlHandler {

}
