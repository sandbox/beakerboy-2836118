<?php

/**
 * @file
 * Contains \Drupal\lab_system\Plugin\Field\FieldFormatter\ChemicalResultDefaultFormatter.
 */

namespace Drupal\lab_system\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'chemical_result' formatter.
 *
 * @FieldFormatter(
 *   id = "chemical_result",
 *   label = @Translation("Chemical Result"),
 *   field_types = {
 *     "chemical_result"
 *   }
 * )
 */
class ChemicalResultDefaultFormatter extends EntityReferenceLabelFormatter{

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $settings = $this->getSettings();

    $summary[] = t('Displays the Result.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    return $elements;  
  }

}

