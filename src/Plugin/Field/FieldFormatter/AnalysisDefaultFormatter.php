<?php

/**
 * @file
 * Contains \Drupal\lab_system\Plugin\Field\FieldFormatter\AnalysisDefaultFormatter.
 */

namespace Drupal\lab_system\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'analysis' formatter.
 *
 * @FieldFormatter(
 *   id = "analysis",
 *   label = @Translation("Analysis"),
 *   field_types = {
 *     "analysis"
 *   }
 * )
 */
class AnalysisDefaultFormatter extends EntityReferenceLabelFormatter{

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $settings = $this->getSettings();

    $summary[] = t('Displays the Analysis.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    foreach ($items as $delta => $item) {
      $entity = $item->entity;
      $label = $item->result;
      //$label = $entity->getName() . ": " . $item->result . ' ' . $entity->getUnits();
      $elements[$delta] = [
        '#markup' => $label,
      ];
    }
    return $elements;
  }
}

