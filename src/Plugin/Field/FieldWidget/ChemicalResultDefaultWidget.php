<?php
/**
 * @file
 * Contains \Drupal\lab_system\Plugin\Field\FieldWidget\ChemicalResultDefaultWidget.
 */

namespace Drupal\lab_system\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Plugin implementation of the 'chemical_result' widget.
 *
 * @FieldWidget(
 *   id = "chemical_result",
 *   module = "lab_system",
 *   label = @Translation("Chemical Result"),
 *   field_types = {
 *     "chemical_result"
 *   }
 * )
 */
class ChemicalResultDefaultWidget extends EntityReferenceAutocompleteWidget{

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $widget = parent::formElement($items, $delta, $element, $form, $form_state);
    $widget['result'] = [
      '#type' => 'number',
      '#title' => t('Result'),
      '#default_value' => isset($items[$delta]->result) ? $items[$delta]->result : 0,
      '#size' => 11,
      '#step' => .0001,
    ];
    
    return $widget;
  }

}

