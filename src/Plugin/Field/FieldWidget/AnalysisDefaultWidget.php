<?php
/**
 * @file
 * Contains \Drupal\lab_system\Plugin\Field\FieldWidget\AnalysisDefaultWidget.
 */

namespace Drupal\lab_system\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Plugin implementation of the 'analysis' widget.
 *
 * @FieldWidget(
 *   id = "analysis",
 *   module = "lab_system",
 *   label = @Translation("Analysis"),
 *   field_types = {
 *     "analysis"
 *   }
 * )
 */
class AnalysisDefaultWidget extends EntityReferenceAutocompleteWidget{

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $widget = parent::formElement($items, $delta, $element, $form, $form_state);
    $widget['date'] = [
      '#type' => 'date',
      '#title' => t('Analysis Date'),
      '#default_value' => isset($items[$delta]->date) ? $items[$delta]->date : '',
    ];
    $widget['result'] = [
      '#type' => 'number',
      '#title' => t('Result'),
      '#default_value' => isset($items[$delta]->result) ? $items[$delta]->result : 0,
      '#size' => 11,
      '#step' => .0001,
    ];
    $widget['note'] = [
      '#type' => 'textarea',
      '#title' => t('Note'),
      '#default_value' => isset($items[$delta]->note) ? $items[$delta]->note: 0,
    ];
    
    return $widget;
  }

}

