<?php
/**
 * @file
 * Contains Drupal\lab_system\Plugin\Field\FieldType\ChemicalResultItem.
 */

namespace Drupal\lab_system\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type of chemical_result
 * 
 * @FieldType(
 *   id = "chemical_result",
 *   label = @Translation("Chemical Result field"),
 *   description = @Translation("Information on a laboratory analysis."),
 *   default_widget = "chemical_result",
 *   default_formatter = "chemical_result",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */

class ChemicalResultItem extends EntityReferenceItem {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);	
    $schema['columns']['result'] = [
      'description' => 'The analysis result.',
      'type' => 'numeric',
      'precision' => '10',
      'scale' => '4',
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['result'] = DataDefinition::create('string')
      ->setLabel(t('Result'))
      ->setDescription(t('The analysis result'))
      ->setRequired(TRUE);
    return $properties;
  }

}

