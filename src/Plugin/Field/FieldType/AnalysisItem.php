<?php
/**
 * @file
 * Contains Drupal\lab_system\Plugin\Field\FieldType\AnalysisItem.
 */

namespace Drupal\lab_system\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Provides a field type of analysis
 * 
 * @FieldType(
 *   id = "analysis",
 *   label = @Translation("Analysis field"),
 *   description = @Translation("Information on a laboratory analysis."),
 *   default_widget = "analysis",
 *   default_formatter = "analysis",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */

class AnalysisItem extends EntityReferenceItem{
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);	
    $schema['columns']['date'] = [
          'description' => 'Analysis Date.',
          'type' => 'int',
      ];
    $schema['columns']['result'] = [
          'description' => 'The analysis result.',
          'type' => 'numeric',
          'precision' => '10',
          'scale' => '4',
        ];
    $schema['columns']['note'] = [
          'description' => 'An analysis note',
          'type' => 'text',
        ];
    return $schema;
  }

  /**
   * {@inheritdoc}
  */
  public function isEmpty() {
    $value1 = $this->get('note')->getValue();
    $value2 = $this->get('result')->getValue();
    $value3 = $this->get('date')->getValue();
    return empty($value1) && empty($value2) && empty($value3) && parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['result'] = DataDefinition::create('string')
      ->setLabel(t('Result'))
      ->setDescription(t('The analysis result'))
      ->setRequired(TRUE);
    $properties['note'] = DataDefinition::create('string')
      ->setLabel(t('Note'))
      ->setDescription(t('A Note'))
      ->setRequired(FALSE);
    $properties['date'] = DataDefinition::create('timestamp')
      ->setLabel(t('Date'))
      ->setDescription(t('The Analysis Date'))
      ->setRequired(TRUE);
    return $properties;
  }

}

