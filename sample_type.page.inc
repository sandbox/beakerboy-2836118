<?php

/**
 * @file
 * Contains sample_type.page.inc.
 *
 * Page callback for Sample Type entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Sample Type templates.
 *
 * Default template: sample_type.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_sample_type(array &$variables) {
  // Fetch Sample Type Entity Object.
  $sample_type = $variables['elements']['#sample_type'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
