<?php

/**
 * @file
 * Contains chemical_analysis.page.inc.
 *
 * Page callback for Chemical Analysis entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Chemical Analysis templates.
 *
 * Default template: chemical_analysis.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_chemical_analysis(array &$variables) {
  // Fetch Chemical Analysis Entity Object.
  $chemical_analysis = $variables['elements']['#chemical_analysis'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
