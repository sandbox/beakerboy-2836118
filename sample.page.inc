<?php

/**
 * @file
 * Contains sample.page.inc.
 *
 * Page callback for Sample entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Sample templates.
 *
 * Default template: sample.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_sample(array &$variables) {
  // Fetch Sample Entity Object.
  $sample = $variables['elements']['#sample'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
