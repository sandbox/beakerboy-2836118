<?php

/**
 * @file
 * Contains chemical_result.page.inc.
 *
 * Page callback for ChemicalResult entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for ChemicalResult templates.
 *
 * Default template: chemical_result.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_chemical_result(array &$variables) {
  // Fetch ChemicalResult Entity Object.
  $chemical_result = $variables['elements']['#chemical_result'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
