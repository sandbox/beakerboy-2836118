The lab system module allows a lab to save information on laboratory samples and their analysis:

Entites:
Chemical - An entity to record information on chemical compounds including names, boiling point, structure, and taxonomy.
Methods - a list of analysis methods and the units thats are returned
SampleType - a list of the types of samples
Sample - a specific sample which will be analyzed. This entity includes the type of sample, when the sample was taken, and from where. A lot number can be given as well.
Analysis - This entity records what method was applied to the sample, when it was performed, and what the result was.
ChemicalAnalysis - For methods which return results of many chemical entities, this saves multiple chemicals
