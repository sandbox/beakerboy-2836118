<?php

/**
 * @file
 * Contains analysis.page.inc.
 *
 * Page callback for Analysis entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for Analysis templates.
 *
 * Default template: analysis.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_analysis(array &$variables) {
  // Fetch Analysis Entity Object.
  $analysis = $variables['elements']['#analysis'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
