<?php

/**
 * @file
 * Contains method.page.inc.
 *
 * Page callback for Method entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Method templates.
 *
 * Default template: method.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_method(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
